// @flow
import React from 'react';
import { Dimensions, FlatList } from 'react-native';
import styled from 'styled-components';
import { DateTime } from 'luxon';
import debounce from 'lodash/debounce';
import { isIphoneX } from 'react-native-iphone-x-helper';
import EventDialog from './EventDialog';

const times = [
  '12:00 AM',
  '01:00 AM',
  '02:00 AM',
  '03:00 AM',
  '04:00 AM',
  '05:00 AM',
  '06:00 AM',
  '07:00 AM',
  '08:00 AM',
  '09:00 AM',
  '10:00 AM',
  '11:00 AM',
  'Noon',
  '01:00 PM',
  '02:00 PM',
  '03:00 PM',
  '04:00 PM',
  '05:00 PM',
  '06:00 PM',
  '08:00 PM',
  '09:00 PM',
  '10:00 PM',
  '11:00 PM',
];

type Props = {
  date: string,
  events: Object,
  saveEvent: Function,
};

type State = {
  daysAdd: number,
  orientation: 'portrait' | 'landscape',
  modalVisible: boolean,
  modalText: string,
  selectedDate: string,
  selectedTime: number,
  month: String,
};

class Calendar extends React.Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      daysAdd: 0,
      orientation: 'portrait',
      modalVisible: false,
      modalText: '',
      selectedDate: '',
      selectedTime: '',
      month: ''
    };
  }

  render() {
    return (
      <>
        <Container onLayout={this.detectOrentation}>
          <Row>
            <Row marginLeft={10} marginTop={10} marginBottom={10} flex={1}>
              <Label>{this.getMonth()}</Label>
            </Row>
            <Row marginRight={10} marginTop={10} marginBottom={10}>
              <Button onPress={() => this.increment(-1)}><Label>{'\<'}</Label></Button>
              <Button onPress={() => this.reset()}><Label>{'Today'}</Label></Button>
              <Button onPress={() => this.increment(1)}><Label>{'\>'}</Label></Button>
            </Row>
          </Row>
          {this.renderDays()}
        </Container>
        <EventDialog visible={this.state.modalVisible} value={this.state.modalText} close={this.closeModal}
                     onChangeText={this.onModalTextChange}
                     saveEvent={this.saveEvent}/>
      </>
    );
  }

  renderDays = () => {
    const cellWidth = this.getCellWidth();
    const dates = this.getDates();

    return (
      <>
        <Row marginLeft={cellWidth}>
          {dates.map((date: Object) =>
            <Cell key={date.value} width={cellWidth} backgroundColor="#3498db" borderColor="white">
              <Label textColor="white">{date.label}</Label>
            </Cell>
          )}
        </Row>
        <FlatList
          data={times}
          extraData={{ events: this.props.events, orientation: this.state.orientation }}
          renderItem={this.renderTimeRow(dates)}
          keyExtractor={this.renderTimeRowKey}/>
      </>

    );
  };

  renderTimeRow = (dates) => ({ item, index }) => {
    return (
      <Row>
        <Cell width={this.getCellWidth()} backgroundColor="#EB984E" borderColor="white">
          <Label textColor="white">{item}</Label>
        </Cell>
        {dates.map((date) => (
          <Cell key={`${date.value}${index}`} width={this.getCellWidth()}>
            <CellButton onPress={() => this.onCellPress(date.value, index)}>
              <Label>{this.getEventData(date.value, times[index])}</Label>
            </CellButton>
          </Cell>
        ))}
      </Row>
    );
  };

  renderTimeRowKey = (item) => item;

  reset = () => this.setState({ daysAdd: 0 });

  getDates = () => {
    const cellsPerRow = this.getCellsPerRow();
    const startDate = this.getStartDate();

    return Array.from(Array(cellsPerRow - 1).keys()).map((index: number) => {
      const date = startDate.plus({ day: index });
      return {
        label: date.toFormat('ccc d'),
        value: date.toFormat('y-MM-dd'),
      };
    });
  };

  getStartDate = () => {
    const { daysAdd, orientation } = this.state;

    let startDate = DateTime.fromISO(this.props.date).startOf('week').minus({ day: 1 }).plus({ day: this.adjustDays() + daysAdd * 5 });

    if (orientation === 'landscape') {
      startDate = startDate.startOf('week').minus({ day: 1 });
    }

    return startDate;
  };

  increment = (num: number) => this.setState(
    (state) => ({
      daysAdd: state.daysAdd + num
    })
  );

  detectOrentation = debounce(() => {
    const { width, height } = Dimensions.get('window');

    this.setState({
      orientation: width > height ? 'landscape' : 'portrait',
    });
  }, 10);

  getMonth = () => {
    const dates = this.getDates();
    const monthStart = DateTime.fromISO(dates[0].value).toFormat('MMMM');
    const monthEnd = DateTime.fromISO(dates[dates.length - 1].value).toFormat('MMMM');
    return monthStart === monthEnd ? monthStart : monthStart + ' - ' + monthEnd;
  };

  adjustDays = () => {
    const dayOfWeek = DateTime.fromISO(this.props.date).toFormat('c');
    return dayOfWeek >= 5 ? Math.abs(5 - dayOfWeek) + 1 : 0;
  };

  getCellsPerRow = () => this.state.orientation === 'portrait' ? 6 : 8;
  getCellWidth = () => (Dimensions.get('window').width - (this.state.orientation === 'landscape' && isIphoneX() ? 80 : 0)) / this.getCellsPerRow();

  closeModal = () => {
    this.setState({ modalVisible: false, modalText: '' });
  };

  onModalTextChange = (event: string) => {
    this.setState({ modalText: event });
  };

  getEventData = (date, time) => {
    const data = this.props.events[date] || {};
    return data[time];
  };

  onCellPress = (date, timeIndex) => {
    this.setState({
      selectedDate: date,
      selectedTime: times[timeIndex],
      modalVisible: true,
      modalText: this.getEventData(date, times[timeIndex]),
    });
  };

  saveEvent = () => {
    const { selectedDate, selectedTime, modalText } = this.state;

    this.props.saveEvent(modalText, selectedDate, selectedTime);

    this.setState({
      modalVisible: false,
      modalText: '',
    });
  };
}

const CellButton = styled.TouchableOpacity`
  width: 100%;
  height: 100%;
  justify-content:center;
`;

const Button = styled.TouchableOpacity`
  border : 0.5px solid #E5E7E9;
  border-radius : 5px;
  padding : 10px;
`;

const Label = styled.Text`
  color : ${props => props.textColor || 'black'};
  font-weight : bold;
  text-align : center;
`;

const Container = styled.View`
  flex: 1;
`;

const Row = styled.View`
  ${ props => props.flex && `flex:${props.flex}`}
  flex-direction: row;
  align-items : center;
  align-self : ${props => props.alignSelf || 'auto'}
  margin-left : ${props => props.marginLeft || 0}px
  margin-right : ${props => props.marginRight || 0}px
  margin-top : ${props => props.marginTop || 0}px
  margin-bottom : ${props => props.marginBottom || 0}px
    
`;

const Cell = styled.View`
  width: ${props => props.width}px;
  height: 40px;
  background-color:${props => props.backgroundColor || 'white'};
  border: 0.5px solid ${props => props.borderColor || '#E5E7E9'};
  justify-content: center;
  align-items: center;
`;

export default Calendar;
