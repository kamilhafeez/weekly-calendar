// @flow
import React from 'react';
import styled from 'styled-components';
import { Modal } from 'react-native';

type Props = {
  close: Function,
  onChangeText: Function,
  value: string,
  saveEvent: Function,
  visible: boolean,
}

class EventDialog extends React.Component<Props> {
  onClose = () => {
    this.props.close();
  };

  render() {
    return <Modal visible={this.props.visible}
                  animationType="fade"
                  onRequestClose={this.onClose} transparent>
      <Container>
        <Background>
          <Label>Enter Event Details</Label>
          <Input value={this.props.value} onChangeText={this.props.onChangeText}/>
          <Button onPress={this.props.saveEvent}>
            <Label color="white">Save</Label>
          </Button>
        </Background>
      </Container>
    </Modal>;
  }
}

const Container = styled.View`
  flex: 1;
  background-color: rgba(0,0,0,.6);
  justify-content: center;
  align-items: center;
`;

const Background = styled.View`
  height: 200px;
  width: 80%;
  background-color: white;
  border-radius: 10px;
  padding: 20px;
  justify-content: center;
  align-items: center;
`;

const Label = styled.Text`
  text-align: center;
  font-weight: bold;
  color: ${props => props.color || 'black'}
`;

const Input = styled.TextInput`
  border-color: grey;
  border-width: 1px;
  border-radius: 5px;
  height: 50px;
  width: 100%;
  padding: 10px;
  margin-top: 20px
`;

const Button = styled.TouchableOpacity`
  width: 140px;
  height: 50px;
  background-color: grey;
  border-radius: 10px;
  margin-top: 20px;
  align-items: center;
  justify-content: center;
`;

export default EventDialog;