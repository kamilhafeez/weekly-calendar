import React from 'react';
import { SafeAreaView } from 'react-native';
import { connect } from 'react-redux';
import { DateTime } from 'luxon';

import Calendar from '../../components/Calendar';
import { saveEvent, loadEvents } from './actions';

type Props = {
  events: Object,
  saveEvent: Function,
  loadEvents: Function,
}

class Home extends React.Component<Props> {
  componentDidMount() {
    this.props.loadEvents();
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <Calendar events={this.props.events} saveEvent={this.props.saveEvent} date={DateTime.local().toFormat('y-MM-dd')}/>
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    events: state.home.events,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    saveEvent: (event, date, time) => dispatch(saveEvent(event, date, time)),
    loadEvents: () => dispatch(loadEvents()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
