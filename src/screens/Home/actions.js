//@flow
import { SAVE_EVENT, EVENTS_LOADED } from './constants';
import { readEvents, storeEvents } from '../../storage';

export const saveEvent = (event: string, date: string, time: string) => (dispatch, getState) => {
  dispatch({
    type: SAVE_EVENT,
    event,
    date,
    time
  });

  storeEvents(getState().home.events);
};

export const loadEvents = () => async (dispatch) => {
  const events = await readEvents();
  dispatch({ type: EVENTS_LOADED, events });
};
