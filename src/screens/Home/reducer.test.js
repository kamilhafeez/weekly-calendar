import 'react-native';
import React from 'react';

import reducer from './reducer';
import { EVENTS_LOADED, SAVE_EVENT } from './constants';

it('should test load events', () => {
  const state = { events: {} };
  const events = { '2019-07-26': { '06:00 PM': 'hello world' } };
  const action = { type: EVENTS_LOADED, events };
  const newState = reducer(state, action);

  expect(newState).toEqual({
    events
  });
});


it('test save events', () => {
  const state = { events: {} };
  const action = { type: SAVE_EVENT, event: 'party one', time: '12:00 AM', date: '2019-07-26' };
  const newState = reducer(state, action);

  expect(newState).toEqual({
    events: {
      '2019-07-26': {
        '12:00 AM': 'party one',
      }
    }
  });
});

