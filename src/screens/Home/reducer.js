import { EVENTS_LOADED, SAVE_EVENT } from './constants';

const INITIAL_STATE = {
  events: {},

};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case EVENTS_LOADED:
      return {
        ...state,
        events: action.events,
      };
    case SAVE_EVENT:
      const { date, time, event } = action;

      return {
        ...state,
        events: {
          ...state.events,
          [date]: {
            ...state.events[date],
            [time]: event,
          }
        }
      };
    default:
      return state;
  }
};
