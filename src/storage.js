import AsyncStorage from '@react-native-community/async-storage';

const EVENT_KEY = 'EVENT_KEY';

export const readEvents = async () => {
  try {
    const data = await AsyncStorage.getItem(EVENT_KEY);
    return JSON.parse(data) || {};
  } catch (e) {

  }

  return {};
};

export const storeEvents = (data) =>
  AsyncStorage.setItem(EVENT_KEY, JSON.stringify(data));
