import React from 'react';
import { Provider } from 'react-redux';

import Home from './screens/Home/index';
import store from './store';


class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Home/>
      </Provider>

    );
  }
}

export default App;